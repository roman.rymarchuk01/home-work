import "./App.css";
import React from "react";
import List from "./components/List/List";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header"

class App extends React.Component {
    state = {
        cards: [],
        cart: [],
        renderModal: false,
        cartCount: 0,
        isFavorite: false,
        favorite: []
    };


    changeFavorite = (article) => {
        const { favorite } = this.state;
        if (favorite.includes(article)) {
            const index = favorite.indexOf(article);
            favorite.splice(index, 1);
        } else {
            favorite.push(article);
        }
        this.setState({ favorite: favorite });
        localStorage.setItem('favorite', JSON.stringify(favorite));
    }


    addToCart = (card) => {
        const cart = this.state.cart;
        const index = cart.findIndex((el) => el.article === card.article);

        if (index === -1) {
            cart.push(card)
            this.setState({ cart: [...cart] });
        } else {
            cart[index].count += 1;
            this.setState({ cart: [...cart] });
        }

        const cartCount = cart.reduce((sum, current) => +sum + current.count, 0);
        this.setState({ cartCount: cartCount })

        console.log(this.state.cart);

        localStorage.setItem("cart", JSON.stringify(this.state.cart));
        localStorage.setItem("cartCount", JSON.stringify(cartCount));

    };

    addToCartModal = ({ header, closeButton, text, actions }) => {
        this.setState({ renderModal: true, header, closeButton, text, actions });
    };

    async componentDidMount() {
        const data = await fetch("./data.json").then((res) => res.json());
        this.setState({ cards: data });

        const cart = localStorage.getItem("cart");
        const cartCount = localStorage.getItem("cartCount")
        const favorite = localStorage.getItem("favorite")

        if (cart) {
            this.setState({ cart: JSON.parse(cart) });
        }

        if (cartCount) {
            this.setState({ cartCount: JSON.parse(cartCount) });

        }

        if (favorite) {
            this.setState({ favorite: JSON.parse(favorite) });

        }
    }

    closeModal = () => {
        this.setState(() => ({
            renderModal: false,
        }));
    };

    render() {
        const { cards, cartCount, isFavorite, favorite } = this.state;



        return (
            <div className="App">
                <Header cartCount={cartCount} />
                <List
                    favorite={favorite}
                    changeFavorite={this.changeFavorite}
                    isFavorite={isFavorite}
                    addToCart={this.addToCart}
                    closeModal={this.closeModal}
                    addToCartModal={this.addToCartModal}
                    cards={cards}
                />

                {this.state.renderModal && (
                    <Modal
                        closeModal={this.closeModal}
                        header={this.state.header}
                        closeButton={this.state.closeButton}
                        text={this.state.text}
                        actions={this.state.actions}
                    />
                )}
            </div>
        );
    }
}

export default App;
