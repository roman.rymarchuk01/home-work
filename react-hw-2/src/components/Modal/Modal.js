import React from "react";
import style from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends React.Component {
  render() {
    const { closeModal, header, closeButton, text, actions } = this.props;

    return (
      <>
        <div
          className={style.modal}
          onClick={(e) => {
            if (e.target.className === style.modal) {
              closeModal();
            }
          }}
        >
          <div className={style.modalBox}>
            {closeButton ? (
              <button onClick={closeModal} className={style.modalCloseBtn}>
                &times;
              </button>
            ) : (
              ""
            )}
            <h2 className={style.modalHeader}>{header}</h2>
            <p className={style.modalText}>{text}</p>
            <div className={style.btnContainer}>{actions}</div>
          </div>
        </div>
      </>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  actions: PropTypes.node,
  text: PropTypes.string.isRequired,
};

Modal.defaultProps = {
  closeButton: false,
  actions: "",
};

export default Modal;
