import React from "react";
import Button from "../Button/Button";
import style from "./Card.module.scss";
import PropTypes from "prop-types";

class Card extends React.Component {

    state = {
        isFavorite: this.props.isFavorite,
    }

    render() {
        const {
            isFavorite,
            header,
            src,
            price,
            article,
            color,
            addToCartModal,
            closeModal,
            addToCart,
            changeFavorite,
        } = this.props;



        return (
            <>
                <h2 className={style.header}>{header}</h2>
                <svg onClick={() => {
                    this.setState({ isFavorite: !isFavorite })
                    changeFavorite(article)
                }} className={isFavorite ? style.notFavorite : style.favorite} xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                    <path
                        d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
                </svg>                <img src={src} alt={header} width={250} height={250} />
                <div className="">
                    <span className={style.price}>{price}</span>
                    <span className={style.article}>art:{article}</span>
                    <span className={style.color} style={{ backgroundColor: color }}></span>
                </div>
                <Button
                    text="Add to cart"
                    onClick={() => {
                        addToCartModal({
                            header: "Attention!!!",
                            text: "Add to cart?",
                            actions: (
                                <>
                                    <Button
                                        onClick={() => {
                                            addToCart({ header, src, price, article, count: 1 });
                                            closeModal();
                                        }}
                                        backgroundColor={"yellow"}
                                        text={"Yes"}
                                    />
                                    <Button
                                        onClick={closeModal}
                                        backgroundColor={"yellow"}
                                        text={"No"}
                                    />
                                </>
                            ),
                        });
                    }}
                ></Button>
            </>
        );
    }
}

Card.propTypes = {
    isFavorite: PropTypes.bool,
    header: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    addToCartModal: PropTypes.func,
    closeModal: PropTypes.func,
    addToCart: PropTypes.func,
};

Card.defaultProps = {
    isFavorite: false,
    addToCartModal: () => { },
    closeModal: () => { },
    addToCart: () => { },
};

export default Card;
