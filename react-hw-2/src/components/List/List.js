import React from "react";
import style from "./List.module.scss";
import PropTypes from "prop-types";
import Card from "../Card/Card";

class List extends React.Component {
    render() {
        const { cards, addToCartModal, closeModal, addToCart, changeFavorite, favorite } = this.props;

        return (
            <main>
                <ul className={style.list}>
                    {cards.map(({ header, src, price, article, color }) => {
                        return (
                            <li key={article} className={style.listItem}>
                                <Card
                                    changeFavorite={changeFavorite}
                                    isFavorite={favorite.includes(article)}
                                    addToCart={addToCart}
                                    closeModal={closeModal}
                                    addToCartModal={addToCartModal}
                                    header={header}
                                    src={src}
                                    price={price}
                                    article={article}
                                    color={color}
                                />
                            </li>
                        );
                    })}
                </ul>
            </main>
        );
    }
}


List.propTypes = {
    changeFavorite: PropTypes.func,
    isFavorite: PropTypes.bool,
    cards: PropTypes.array.isRequired,
    addToCartModal: PropTypes.func,
    closeModal: PropTypes.func,
    addToCart: PropTypes.func,
};

List.defaultProps = {
    changeFavorite: () => { },
    isFavorite: false,
    addToCartModal: () => { },
    closeModal: () => { },
    addToCart: () => { },
};

export default List;
