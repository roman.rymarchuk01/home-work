import React from "react";
import style from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends React.Component {
  render() {
    const { backgroundColor, text, onClick } = this.props;

    return (
      <button
        className={style.button}
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: "white",
  text: "default button",
  onClick: () => { },
};

export default Button;
