import dataReducer from "./dataReducer";
import { GET_DATA } from "./actions";

const initialState = {
    data: [],
}

describe('dataReducer reducer works', () => {

    test('should return the initial state', () => {
        expect(dataReducer(undefined, { type: undefined })).toEqual(initialState)
      })

      test('should set data', () => {
        expect(dataReducer(initialState, { type: GET_DATA, payload: ['dog', 'cat'] })).toEqual({
            data: ['dog', 'cat'],
        })
      })

    })