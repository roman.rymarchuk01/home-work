import formModalReducer from "./formModalReducer";
import { SET_IS_FORM_MODAL_ACTIVE } from "./actions";

const initialState = {
  isModalActive: false,
}

describe('formModalReducer reducer works', () => {

    test('should return the initial state', () => {
        expect(formModalReducer(undefined, { type: undefined })).toEqual(initialState)
      })

      test('should change isModalActive', () => {
        expect(formModalReducer(initialState, { type: SET_IS_FORM_MODAL_ACTIVE, payload: true })).toEqual({
            isModalActive: true
         })
      })

 })
