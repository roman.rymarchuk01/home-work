import modalReducer from "./modalReducer";
import { SET_IS_MODAL_ACTIVE, ADD_MODAL_PROPS } from "./actions";

const initialState = {
    modalProps: {},
    isModalActive: false,
}

describe('Modal reducer works', () => {

    test('should return the initial state', () => {
        expect(modalReducer(undefined, { type: undefined })).toEqual(initialState)
      })

      test('should change isModalActive', () => {
        expect(modalReducer(initialState, { type: SET_IS_MODAL_ACTIVE, payload: true })).toEqual({
            isModalActive: true,
            modalProps: {}
         })
      })

      test('should add modalProps', () => {
        expect(modalReducer(initialState, { type: ADD_MODAL_PROPS, payload: { test: true } })).toEqual({
            isModalActive: false,
            modalProps: { test: true }
         })
      })

 })
