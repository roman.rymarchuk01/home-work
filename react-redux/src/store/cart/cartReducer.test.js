import cartReducer from "./cartReducer";
import { ADD_TO_CART, SET_CART_COUNT, INCREMENT, DICREMENT, REMOVE_FROM_CART, CLEAN_CART } from "./actions";

const initialState = {
    cart: [],
    cartCount: 0,
}

describe('Cart reducer works', () => {

    test('should return the initial state', () => {
        expect(cartReducer(undefined, { type: undefined })).toEqual(initialState)
    })

    test('should added to cart', () => {
        expect(cartReducer(initialState, { type: ADD_TO_CART, payload: { article: 1 } })).toEqual({
            cart: [{ article: 1, count: 1 }],
            cartCount: 0,
        })
    })


    test('should don`t added to cart, but set count + 1', () => {
        initialState.cart = [{ article: 1, count: 1 }]
         expect(cartReducer(initialState, { type: ADD_TO_CART, payload: { article: 1 } })).toEqual({
            cart: [{ article: 1, count: 2 }],
               cartCount: 0,
        })
        initialState.cart = []

    })

    test('should set cartCount', () => {
        initialState.cart = [{ article: 1, count: 3 }, { article: 2, count: 3 }]
         expect(cartReducer(initialState, { type: SET_CART_COUNT })).toEqual({
            cart: [{ article: 1, count: 3 }, { article: 2, count: 3 }],
               cartCount: 6,
        })
        initialState.cart = []

    })

    test('should INCREMENT work', () => {
        initialState.cart = [{ article: 1, count: 3 }]
         expect(cartReducer(initialState, { type: INCREMENT,  payload: 1 })).toEqual({
            cart: [{ article: 1, count: 4 }],
               cartCount: 0,
        })
        initialState.cart = []

    })

    test('should DICREMENT work', () => {
        initialState.cart = [{ article: 1, count: 3 }]
         expect(cartReducer(initialState, { type: DICREMENT,  payload: 1 })).toEqual({
            cart: [{ article: 1, count: 2 }],
               cartCount: 0,
        })
        initialState.cart = []

    })

    test('should REMOVE_FROM_CART work', () => {
        initialState.cart = [{ article: 1, count: 3 }]
         expect(cartReducer(initialState, { type: REMOVE_FROM_CART,  payload: 1 })).toEqual({
            cart: [],
            cartCount: 0,
        })
        initialState.cart = []

    })

    test('should CLEAN_CART work', () => {
        initialState.cart = [{ article: 1, count: 3 }, { article: 4, count: 1 }, { article: 2, count: 3 }]
         expect(cartReducer(initialState, { type: CLEAN_CART,  payload: 1 })).toEqual({
            cart: [],
            cartCount: 0,
        })
        initialState.cart = []

    })
})

