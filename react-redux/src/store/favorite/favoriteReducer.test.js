import { ADD_TO_FAVORITE } from "./actions";
import favoriteReducer from "./favoriteReducer";

const initialState = {
    favorite: [],
    isFavorite: false,
}

describe('Favorite reducer works', () => {

    test('should return the initial state', () => {
        expect(favoriteReducer(undefined, { type: undefined })).toEqual(initialState)
    })

    test('should added to cart', () => {
        expect(favoriteReducer(initialState, { type: ADD_TO_FAVORITE, payload: "cat" })).toEqual({
            favorite: ["cat"],
            isFavorite: true,
        })
    })

})


