import { render } from "@testing-library/react"
import { Formik } from "formik"
import CustomInput from "./CustomInput"

const Component = () => {
    return (
        <Formik
            initialValues={{ someInput: '' }}
            handleSubmit={jest.fn()}

        >
            <CustomInput lable="some input" name="someInput" />
        </Formik>
    )
}

describe("Render CustomInput", () => {

    test("Should CustomInput render", () => {

        const { asFragment } = render(<Component />)
        expect(asFragment()).toMatchSnapshot()

    })

})