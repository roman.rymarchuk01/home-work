import { render, screen, fireEvent } from "@testing-library/react"
import { setIsModalActivelAC } from "../../store/modal/actionCreators"
import '@testing-library/jest-dom'
import Modal from "./Modal"
import { Provider, useDispatch } from "react-redux"
import store from "../../store"

const Component = () => {
    const dispatch = useDispatch();

    const modalProps = {
        title: 'modal',
        closeButton: true,
        actions: <div>test</div>
    }

    return (
        <>
            <button onClick={
                () => { dispatch(setIsModalActivelAC(true)) }}
            >Button</button>
            <Modal modalProps={modalProps} />
        </>
    )
}

const MockedProvider = () => {

    return (
        <Provider store={store} >
            <Component />
        </Provider>
    )
}



describe("Render Modal", () => {

    test("should Modal render corect", () => {
        const { asFragment } = render(<MockedProvider />);
        expect(asFragment()).toMatchSnapshot()
    })
})




describe('Modal test', () => {
    test('should Modal close on background click', () => {
        render(<MockedProvider />);

        fireEvent.click(screen.getByText('Button'));
        expect(screen.getByTestId('modal')).toBeInTheDocument();

    });

    test('should Modal render actions', () => {
        render(<MockedProvider />);

        expect(screen.getByText('test')).toBeInTheDocument();

    });

    test('should Modal render closeButton', () => {
        render(<MockedProvider />);

        expect(screen.getByTestId('close-btn')).toBeInTheDocument();

    });

    test('should Modal title render', () => {
        render(<MockedProvider />);

        expect(screen.getByText('modal')).toBeInTheDocument();

    });

    // test('should Modal title render', () => {
    //     render(<MockedProvider />);

    //     fireEvent.click(screen.getByText('Button'));
    //     fireEvent.click(screen.getByTestId('modal'))
    //     expect(screen.getByTestId('modal')).not.toBeInTheDocument();

    // });
})





