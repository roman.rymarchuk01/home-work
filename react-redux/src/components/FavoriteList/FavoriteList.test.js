import { render, screen, fireEvent } from "@testing-library/react"
import { Provider } from "react-redux"
import FavoriteList from "./FavoriteList"
import store from "../../store"

const Component = () => {
    return (
        <Provider store={store} >
            <FavoriteList />
        </Provider>
    )
}

describe("Render FavoriteList", () => {
    test('Should FavoriteList render', () => {
        const { asFragment } = render(<Component />)
        expect(asFragment()).toMatchSnapshot()

    })
})

