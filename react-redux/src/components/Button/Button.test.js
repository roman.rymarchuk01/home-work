import { render, screen, fireEvent } from "@testing-library/react"
import Button from "./Button"

const handleClick = jest.fn()

describe("Render Button", () => {

    test("should button render", () => {
        const { asFragment } = render(<Button>BUTTON</Button>);
        expect(asFragment()).toMatchSnapshot()
    })

})

describe("Handle click at a button", () => {

    test("should handleClick work", () => {
        render(<Button handleClick={handleClick} >BUTTON</Button>);

        const btn = screen.getByText("BUTTON");

        fireEvent.click(btn)
        expect(handleClick).toHaveBeenCalled();
    })


})