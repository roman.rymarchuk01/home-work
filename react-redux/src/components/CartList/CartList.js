import React, { memo, useContext, useState } from 'react';
import CartItem from '../CartItem/CartItem';
import PropTypes from "prop-types";
import { Context } from '../../context';

import "./CartList.scss"



const CartList = (props) => {

    const { cart } = props
    const { grid, flex } = useContext(Context)

    const [isGrid, setIsGrid] = useState(false)

    return (

        <section>
            <ul className={`cart-list `}>
                {cart.map(({ article, src, count, header }) => <CartItem key={article} article={article} src={src} header={header} count={count} listDesing={isGrid ? grid : flex} />)}
            </ul>
            <button onClick={() => setIsGrid(!isGrid)} >Change Style</button>
        </section>

    );
}

CartList.propTypes = {
    cart: PropTypes.array.isRequired,
};


export default memo(CartList);
