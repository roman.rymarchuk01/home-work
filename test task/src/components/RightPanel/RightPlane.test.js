import { render } from "@testing-library/react"
import RightPanel from "./RightPanel"


describe("Render RightPanel", () => {

    test("should RightPanel render", () => {
        const { asFragment } = render(<RightPanel photoId={4} />);
        expect(asFragment()).toMatchSnapshot()
    })

})