import React from 'react';
import style from './RightPanel.module.scss';


const RightPanel = ({ photoId }) => {
    return (
        <section style={{ background: `url(../../photo-${photoId[3]}.jpg) 100% 100%/100% 100% no-repeat` }} className={style.wrapper} >
            <br />
        </section>
    );
}

export default RightPanel;
