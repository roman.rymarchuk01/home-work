import { render } from "@testing-library/react"
import Footer from "./Footer"


describe("Render Footer", () => {

    test("should Footer render", () => {
        const { asFragment } = render(<Footer photoId={1} />);
        expect(asFragment()).toMatchSnapshot()
    })

})