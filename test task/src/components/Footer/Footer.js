import React from 'react';
import style from './Footer.module.scss';

const Footer = ({ photoId }) => {
    return (
        <footer style={{ background: `url(../../photo-${photoId[2]}.jpg) 100% 100%/100% 100% no-repeat` }} className={style.wrapper} >
            <br />
        </footer>
    );
}

export default Footer;
