import React from 'react';
import style from './LeftPanel.module.scss';


const LeftPanel = ({ photoId }) => {
    return (
        <section style={{ background: `url(../../photo-${photoId[4]}.jpg) 100% 100%/100% 100% no-repeat` }} className={style.wrapper} >
            <br />
        </section>
    );
}

export default LeftPanel;
