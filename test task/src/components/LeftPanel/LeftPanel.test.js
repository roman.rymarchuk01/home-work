import { render } from "@testing-library/react"
import LeftPanel from "./LeftPanel"


describe("Render LeftPanel", () => {

    test("should LeftPanel render", () => {
        const { asFragment } = render(<LeftPanel photoId={0} />);
        expect(asFragment()).toMatchSnapshot()
    })

})