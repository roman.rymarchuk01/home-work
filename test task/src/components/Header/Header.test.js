import { render } from "@testing-library/react"
import Header from "./Header"


describe("Render Header", () => {

    test("should Header render", () => {
        const { asFragment } = render(<Header photoId={2} />);
        expect(asFragment()).toMatchSnapshot()
    })

})