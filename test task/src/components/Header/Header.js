import React, { useState } from 'react';
import style from './Header.module.scss';


const Header = ({ photoId }) => {
    return (
        <header style={{ background: `url(../../photo-${photoId[1]}.jpg) 100% 100%/100% 100% no-repeat` }} className={style.wrapper} >
            <br />
        </header>
    );
}

export default Header;
