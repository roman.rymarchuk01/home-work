import { render } from "@testing-library/react"
import MainBody from "./MainBody"


describe("Render MainBody", () => {

    test("should MainBody render", () => {
        const { asFragment } = render(<MainBody photoId={3} />);
        expect(asFragment()).toMatchSnapshot()
    })

})