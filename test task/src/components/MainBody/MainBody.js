import React, { useState, useEffect } from 'react';
import style from './MainBody.module.scss';


const MainBody = ({ handleClick, photoId }) => {

    const [btnColor, setBtnColor] = useState('white')

    const changeBtnColor = () => {
        setBtnColor('#' + (Math.random().toString(16) + '000000').substring(2, 8).toUpperCase())
    }

    useEffect(() => {
        setInterval(() => {
            handleClick(); changeBtnColor();
        }, 520)
    }, [])


    return (
        <section style={{ background: `url(../../photo-${photoId[0]}.jpg) 100% 100%/100% 100% no-repeat` }} className={style.wrapper} >
            <button
                className={style.btn}
                style={{
                    backgroundColor: btnColor
                }} onClick={() => { handleClick(); changeBtnColor(); }} > Change background images </button>
        </section>
    );
}

export default MainBody;
