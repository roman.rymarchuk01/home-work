import React from 'react';
import LeftPanel from '../LeftPanel/LeftPanel';
import MainBody from '../MainBody/MainBody';
import RightPanel from '../RightPanel/RightPanel';
import style from './Main.module.scss';


const Main = ({ handleClick, photoId }) => {
    return (
        <main className={style.wrapper} >
            <br />
            <LeftPanel photoId={photoId} />
            <MainBody photoId={photoId} handleClick={handleClick} />
            <RightPanel photoId={photoId} />
            <br />

        </main>
    );
}

export default Main;
