import { render } from "@testing-library/react"
import Main from "./Main"

const handleClick = jest.fn()


describe("Render Main", () => {

    test("should Main render", () => {
        const { asFragment } = render(<Main handleClick={handleClick} photoId={[]} />);
        expect(asFragment()).toMatchSnapshot()
    })

})