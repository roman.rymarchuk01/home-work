import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import { useState } from 'react'

function App() {

    const [photoId, setPhotoId] = useState([1, 2, 3, 4, 5])

    const change = () => {
        const currentPhotoIdArr = photoId;
        const mockPhotoIdArr = currentPhotoIdArr.map(el => el = null);
        const newPhotoIdArr = mockPhotoIdArr.map((el, index, arr) => {


            const changeId = () => {
                const newId = Math.floor(Math.random() * 6)

                if (newId === 0 || newId === photoId[index] || arr.includes(newId)) {
                    return changeId()
                }
                arr[index] = newId
                return newId
            }

            return el = changeId()
        })
        setPhotoId(newPhotoIdArr)
    }

    return (
        <>
            <Header photoId={photoId} />
            <Main photoId={photoId} handleClick={change} />
            <Footer photoId={photoId} />
        </>
    );
}

export default App;
