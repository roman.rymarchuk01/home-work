import "./App.css";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
  state = {
    renderModal: false,
  };

  onClick = ({ header, closeButton, text, actions }) => {
    this.setState(() => ({
      renderModal: true,
      header,
      closeButton,
      text,
      actions,
    }));
  };

  closeModal = () => {
    this.setState(() => ({
      renderModal: false,
    }));
  };

  render() {
    return (
      <div className="App">
        <Button
          backgroundColor="red"
          text="Open first modal"
          onClick={() => {
            this.onClick({
              header: "Do you want to delete this file?",
              closeButton: true,
              text:
                "Once you delete this file, it won’t be possible to undo this action. \n" +
                "Are you sure you want to delete it?",
              actions: (
                <>
                  <Button
                    onClick={this.closeModal}
                    backgroundColor={"rgba(61, 59, 59, 0.64)"}
                    text={"Ok"}
                  />
                  <Button
                    onClick={this.closeModal}
                    backgroundColor={"rgba(61, 59, 59, 0.64)"}
                    text={"Delete"}
                  />
                </>
              ),
            });
          }}
        ></Button>

        <Button
          backgroundColor="blue"
          text="Open second modal"
          onClick={() => {
            this.onClick({
              header: "header",
              closeButton: false,
              text: "some text",
              actions: (
                <>
                  <Button
                    onClick={this.closeModal}
                    backgroundColor={"gray"}
                    text={"Ok"}
                  />
                  <Button
                    onClick={this.closeModal}
                    backgroundColor={"gray"}
                    text={"Delete"}
                  />
                </>
              ),
            });
          }}
        ></Button>

        {this.state.renderModal && (
          <Modal
            closeModal={this.closeModal}
            header={this.state.header}
            closeButton={this.state.closeButton}
            text={this.state.text}
            actions={this.state.actions}
          />
        )}
      </div>
    );
  }
}
export default App;
