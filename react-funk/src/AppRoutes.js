import { Routes, Route } from 'react-router-dom';
import StorePage from './pages/StorePage/StorePage';
import CartPage from './pages/CartPage/CartPage';
import FavoritePage from './pages/FavoritePage/FavoritePage';
import PropTypes from "prop-types";


const AppRoutes = (props) => {


    const { favorite, closeModal, addToCart, removeFromCart, increment, dicrement, cart, addToFavorite, setModalProps, setIsModalActive, data } = props




    return (
        <Routes>

            <Route path='/' element={<StorePage favorite={favorite} closeModal={closeModal} addToCart={addToCart} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} data={data} />} />

            <Route path='/cart' element={<CartPage closeModal={closeModal} setModalProps={setModalProps} setIsModalActive={setIsModalActive} removeFromCart={removeFromCart} increment={increment} cart={cart} dicrement={dicrement} />} />

            <Route path='/favorite' element={<FavoritePage closeModal={closeModal} addToCart={addToCart} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} data={data} favorite={favorite} />} />

        </Routes>
    )
}

AppRoutes.propTypes = {
    favorite: PropTypes.array.isRequired,
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func.isRequired,
    increment: PropTypes.func.isRequired,
    dicrement: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
    addToFavorite: PropTypes.func,
    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

AppRoutes.defaultProps = {
    addToFavorite: () => { },
    addToCart: () => { },
    setModalProps: () => { },
    setIsModalActive: () => { },
};

export default AppRoutes;