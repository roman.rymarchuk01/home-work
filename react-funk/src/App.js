import './App.css';
import Header from './components/Header/Header';
import React, { useEffect, useState } from 'react';
import Modal from './components/Modal/Modal';
import AppRoutes from './AppRoutes';



function App() {

    const [data, setData] = useState([])
    const [isModalActive, setIsModalActive] = useState(false)
    const [modalProps, setModalProps] = useState({})
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem("cart")) || [])
    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem("favorite")) || [])
    const [cartCount, setCartCount] = useState(JSON.parse(localStorage.getItem("cartCount")) || 0)
    const [isFavorite, setIsFavorite] = useState(false);


    const addToFavorite = (article) => {
        const currentFavorite = favorite
        if (currentFavorite.includes(article)) {
            const index = currentFavorite.indexOf(article);
            currentFavorite.splice(index, 1);
        } else {
            currentFavorite.push(article);
        }
        setIsFavorite(!isFavorite)
        setFavorite(currentFavorite)
        localStorage.setItem('favorite', JSON.stringify(currentFavorite));

    }


    const closeModal = () => {
        setIsModalActive(false)
    }


    const addToCart = (card) => {
        const index = cart.findIndex(el => el.article === card.article)
        const currentCart = cart;



        if (index === -1) {
            card.count = 1
            currentCart.push(card)
            setCart(currentCart)
        } else {
            currentCart[index].count += 1;

        }

        const newCartCount = currentCart.reduce((sum, current) => sum + current.count, 0);

        setCartCount(newCartCount)

        localStorage.setItem("cart", JSON.stringify(currentCart));
        localStorage.setItem("cartCount", JSON.stringify(newCartCount));
    }


    const dicrement = (article) => {
        const currentCart = cart
        const idx = currentCart.findIndex(el => el.article === article)

        currentCart[idx].count += 1

        setCart(currentCart)


        const newCartCount = currentCart.reduce((sum, current) => sum + current.count, 0);

        setCartCount(newCartCount)

        localStorage.setItem("cart", JSON.stringify(currentCart));
        localStorage.setItem("cartCount", JSON.stringify(newCartCount));

    }



    const increment = (article) => {
        const currentCart = cart
        const idx = currentCart.findIndex(el => el.article === article)

        if (currentCart[idx].count > 1) {
            currentCart[idx].count = currentCart[idx].count - 1
        }

        setCart(currentCart)


        const newCartCount = currentCart.reduce((sum, current) => sum + current.count, 0);

        setCartCount(newCartCount)

        localStorage.setItem("cart", JSON.stringify(currentCart));
        localStorage.setItem("cartCount", JSON.stringify(newCartCount));

    }

    const removeFromCart = (article) => {
        const currentCart = cart
        const idx = currentCart.findIndex(el => el.article === article)

        currentCart.splice(idx, 1)
        setCart(currentCart)


        const newCartCount = currentCart.reduce((sum, current) => sum + current.count, 0);

        setCartCount(newCartCount)

        localStorage.setItem("cart", JSON.stringify(currentCart));
        localStorage.setItem("cartCount", JSON.stringify(newCartCount));
    }



    useEffect(() => {
        const getData = async () => {
            const data = await fetch("./data.json").then(res => res.json())
            setData(data)
        }
        getData()
    }, []);

    return (
        <div className="App">
            <Header cartCount={cartCount} />
            <AppRoutes favorite={favorite} closeModal={closeModal} addToCart={addToCart} removeFromCart={removeFromCart} increment={increment} dicrement={dicrement} cart={cart} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} data={data} />
            {isModalActive && <Modal closeModal={closeModal} modalProps={modalProps} />}
        </div>
    );
}

export default App;
