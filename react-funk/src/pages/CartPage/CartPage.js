import React from 'react';
import CartList from '../../components/CartList/CartList';
import PropTypes from "prop-types";



const CartPage = (props) => {

    const { closeModal, setModalProps, setIsModalActive, removeFromCart, increment, dicrement, cart } = props

    if (cart.length) {

        return (
            <CartList closeModal={closeModal} setModalProps={setModalProps} setIsModalActive={setIsModalActive} removeFromCart={removeFromCart} increment={increment} dicrement={dicrement} cart={cart} />
        );
    } return <h1>Cart is clean</h1>
}


CartPage.propTypes = {
    closeModal: PropTypes.func.isRequired,
    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    removeFromCart: PropTypes.func.isRequired,
    increment: PropTypes.func.isRequired,
    dicrement: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
};

CartPage.defaultProps = {

    setModalProps: () => { },
    setIsModalActive: () => { },

};


export default CartPage;
