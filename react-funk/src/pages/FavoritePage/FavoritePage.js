import React from 'react';
import FavoriteList from '../../components/FavoriteList/FavoriteList';
import PropTypes from "prop-types";


const FavoritePage = (props) => {

    const { closeModal, addToCart, addToFavorite, setModalProps, setIsModalActive, data, favorite, } = props


    if (favorite.length) {
        return (
            <FavoriteList closeModal={closeModal} addToCart={addToCart} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} data={data} favorite={favorite} />
        );
    }
    return <h1>No items</h1>
}

FavoriteList.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func,
    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    data: PropTypes.array.isRequired,
    favorite: PropTypes.array.isRequired,
}
FavoriteList.defaultProps = {
    addToCart: () => { },
    addToFavorite: () => { },
    setModalProps: () => { },
    setIsModalActive: () => { },
}

export default FavoritePage;
