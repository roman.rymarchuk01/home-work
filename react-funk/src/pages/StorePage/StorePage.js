import React from 'react';
import CardList from '../../components/CardList/CardList';
import PropTypes from "prop-types";


const StorePage = (props) => {

    const { favorite, closeModal, addToCart, addToFavorite, setModalProps, setIsModalActive, data } = props



    return (

        <CardList favorite={favorite} closeModal={closeModal} addToCart={addToCart} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} data={data} />
    );
}

CardList.propTypes = {
    favorite: PropTypes.array.isRequired,
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func,
    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

CardList.defaultProps = {
    addToFavorite: () => { },
    addToCart: () => { },
    setModalProps: () => { },
    setIsModalActive: () => { },
};

export default StorePage;
