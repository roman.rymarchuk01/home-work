import React from 'react';
import "./Modal.scss"
import PropTypes from "prop-types";


const Modal = (props) => {

    const { modalProps, closeModal } = props
    const { title, closeButton, actions, } = modalProps


    // console.log(actions);


    return (
        <>
            <div
                className='modal'
                onClick={(e) => {
                    if (e.target.className === "modal") {
                        closeModal();
                    }
                }}
            >
                <div className='modal__wrapper' >
                    {closeButton ? (
                        <button onClick={closeModal} className={"modal__close-btn"}>
                            &times;
                        </button>
                    ) : (
                        ""
                    )}
                    <h2 className='modal__title' >{title}</h2>
                    <p className='modal__text' >Confirm your actions</p>
                    <div className='modal__btn-container'>{actions}</div>
                </div>
            </div>
        </>)
}


Modal.propTypes = {
    modalProps: PropTypes.object.isRequired,
    closeModal: PropTypes.func.isRequired,
}

export default Modal