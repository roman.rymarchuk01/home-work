import React from 'react';
import Button from '../Button/Button';
import "./CartItem.scss"
import PropTypes from "prop-types";


const CartItem = (props) => {

    const { closeModal, setModalProps, setIsModalActive, removeFromCart, increment, dicrement, article, src, header, count, } = props

    const actionsForModal = <><Button handleClick={() => { removeFromCart(article); closeModal(); }} >Delete</Button>
        <Button handleClick={closeModal} >Cancel</Button></>

    const openModal = () => {
        const modalProps = {
            title: `Delete ${header}from cart?`, closeButton: true, actions: actionsForModal,
        }
        setModalProps(modalProps)
        setIsModalActive(true)
    }


    return (

        <li className='cart__item' >
            <img src={src} alt={header} width='100' height={100} />
            <h2 className='cart__header' >{header}</h2>
            <span className='cart__count' >{count}</span>
            <Button handleClick={() => { dicrement(article) }} >+</Button>
            <Button handleClick={() => { increment(article) }}>-</Button>
            <Button handleClick={() => { openModal() }}>DEL</Button>

        </li>


    );
}

CartItem.propTypes = {

    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    closeModal: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    dicrement: PropTypes.func.isRequired,
    increment: PropTypes.func.isRequired,
    article: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired,

};

CartItem.defaultProps = {

    setModalProps: () => { },
    setIsModalActive: () => { },

};

export default CartItem;
