import React from 'react';
import "./Button.scss"
import PropTypes from "prop-types";


const Button = (props) => {

    const { children, handleClick, type } = props;


    return (
        <button type={type} className='button' onClick={handleClick} >{children}</button>
    )
}

Button.propTypes = {
    children: PropTypes.string,
    handleClick: PropTypes.func.isRequired,
}

Button.defaultProps = {
    children: "Button",
    type: 'button'
}



export default Button

