import React from 'react';
import Card from '../Card/Card';
import "./FavoriteList.scss"
import PropTypes from "prop-types";

const FavoriteList = (props) => {

    const { closeModal, addToCart, data, setModalProps, setIsModalActive, addToFavorite, favorite } = props
    const result = data.filter(el => favorite.includes(el.article))

    return (
        <section>
            <ul className='favorite-list' >
                {result.map(el => <Card closeModal={closeModal} addToCart={addToCart} isFavorite={JSON.parse(localStorage.getItem("favorite").includes(el.article))} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} key={el.article} card={el} />)}
            </ul>
        </section>
    );
}

FavoriteList.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func,
    data: PropTypes.array.isRequired,
    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    addToFavorite: PropTypes.func,
    favorite: PropTypes.array.isRequired,
}
FavoriteList.defaultProps = {
    addToCart: () => { },
    setModalProps: () => { },
    setIsModalActive: () => { },
    addToFavorite: () => { },
}

export default FavoriteList;
