import React from 'react';
import Card from '../Card/Card';
import "./CardList.scss"
import PropTypes from "prop-types";



const CardList = (props) => {

    const { favorite, closeModal, addToCart, data, setModalProps, setIsModalActive, addToFavorite, } = props

    return (
        <section>
            <ul className='card-list'>
                {data.map((el) => <Card closeModal={closeModal} addToCart={addToCart} isFavorite={favorite.includes(el.article)} addToFavorite={addToFavorite} setModalProps={setModalProps} setIsModalActive={setIsModalActive} key={el.article} card={el} />)}
            </ul>
        </section>
    )
}

CardList.propTypes = {
    favorite: PropTypes.array.isRequired,
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    addToFavorite: PropTypes.func,


};

CardList.defaultProps = {
    addToCart: () => { },
    setModalProps: () => { },
    setIsModalActive: () => { },
    addToFavorite: () => { },

};



export default CardList