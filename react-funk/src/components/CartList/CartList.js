import React from 'react';
import CartItem from '../CartItem/CartItem';
import "./CartList.scss"
import PropTypes from "prop-types";


const CartList = (props) => {

    const { closeModal, setModalProps, setIsModalActive, removeFromCart, increment, cart, dicrement } = props

    return (

        <section>
            <ul className='cart-list'>
                {cart.map(({ article, src, count, header }) => <CartItem closeModal={closeModal} key={article} setModalProps={setModalProps} setIsModalActive={setIsModalActive} removeFromCart={removeFromCart} increment={increment} article={article} dicrement={dicrement} src={src} header={header} count={count} />)}
            </ul>
        </section>

    );
}

CartList.propTypes = {

    setModalProps: PropTypes.func,
    setIsModalActive: PropTypes.func,
    closeModal: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    dicrement: PropTypes.func.isRequired,
    increment: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
};

CartList.defaultProps = {

    setModalProps: () => { },
    setIsModalActive: () => { },

};

export default CartList;
