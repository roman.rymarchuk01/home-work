"use strict";

const student = {
  table: {},
};

student.name = prompt("Enter your name:", "Roman");
student.lastName = prompt("Enter your last name:", "Rymarchuk");
student.table.subject = [];
student.table.mark = [];

do {
  let subject = prompt("Enter your subject:", "math");
  if (subject === null) {
    break;
  }

  let mark = prompt("Enter your mark", 8);

  student.table.subject.push(subject);
  student.table.mark.push(+mark);
} while (true);

const badMark = student.table.mark.filter((elem) => elem <= 4);

if (badMark.length < 1) {
  console.log("Студент переведено на наступний курс");
}

let markSum = student.table.mark.reduce(
  (previousElem, currentElem) => previousElem + currentElem
);

if (markSum / student.table.mark.length > 7) {
  console.log("Студенту призначено стипендію");
}
