"use strict";
//1.Создать думя способами 1. insertAdjacentHTML 2. createElement или же напрямую в html
//2.первый параметр функции insertAdjacentHTML означает место относительно указаного элемента куда будет встален элемент (afterbegin - после началом элемента, afterend - после конца элемента, beforebegin - перед началом элемента, beforeend - перед концом)
//3.Удалить html элемент можно друмя способами 1. .remove() 2.innerHTML = ""

const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const arr2 = ["1", "2", "3", "sea", "user", 23];

const container = document.querySelector(".container");
const list = document.createElement("ul");
list.className = "list";

container.append(list);

const showArr = (arr, parent = document.body) => {
  const htmlLiColection = arr.map(
    (element) => `<li class="list-item">${element}</li>`
  );

  list.innerHTML = htmlLiColection.join("");
};
showArr(arr1);
