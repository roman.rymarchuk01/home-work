"use strict";

//1.Потому что события клавиатуры не видят вставленый текст или вписаный с помощью виртуальной клавиатуры

window.addEventListener("keyup", (event) => {
  switch (event.code) {
    case "KeyS":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#s").classList.add("active");
      break;
    case "KeyE":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#e").classList.add("active");
      break;
    case "KeyO":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#o").classList.add("active");
      break;
    case "KeyN":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#n").classList.add("active");
      break;
    case "KeyL":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#l").classList.add("active");
      break;
    case "KeyZ":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#z").classList.add("active");
      break;
    case "Enter":
      document.querySelector(".active")?.classList?.remove("active");
      document.querySelector("#enter").classList.add("active");
      break;
  }
});
