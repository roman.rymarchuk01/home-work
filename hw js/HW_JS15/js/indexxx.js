"use strict";

//1.Рекурсия, это когда функция вызывает саму себя, подходит для решения определенных задачь

let number;
do {
  number = prompt("Enter your number", "10");
} while (isNaN(number) || number === null || number === "");

const toFactorial = (num) => {
  if (num === 0) {
    return `Невозможно посчитать фаториал с ${num}`;
  } else if (num === 1) {
    return 1;
  } else {
    return toFactorial(num - 1) * num;
  }
};

//___________________________________________________________________________

// const toFactorial = (num) => {
//   let factorial = num;
//   for (let i = 1; i < num; i++) {
//     factorial = factorial * i;
//   }
//   return factorial;
// };

console.log(toFactorial(number));
