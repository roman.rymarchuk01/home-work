"use strict";

//1. Екранирование в програмировании используется для отображения специальных символов
//2. 1.Стрелочная функция 2.Обьявление функции в переменной (function expression) 3. Функция обьявленая без переменной(function declaration)
//3. Хостинг это процесс который перемещает переменные и обьявленные функции в верх своей области видимости

function createNewUser() {
  let firstName = prompt("Введите Имя");
  let lastName = prompt("Введите фамилию");
  let birthday = prompt(
    "Введите вашу дату рождения в формате дд.мм.гггг",
    "01.01.2000"
  );

  let birthdayDate = new Date(
    `${birthday.slice(6)}-${birthday.slice(3, 5)}-${birthday.slice(0, 2)}`
  );

  let birthdayTime = Date.parse(birthdayDate);

  return {
    firstName,
    lastName,
    birthday,

    getLogin() {
      let firstLetterOfName = firstName.slice(0, 1);
      let login = firstLetterOfName.concat(lastName);
      return `Ваш логин: ${login.toLowerCase()};`;
    },

    getAge() {
      let newDateTime = Date.parse(new Date());
      let age = (newDateTime - birthdayTime) / 1000 / 60 / 60 / 24 / 365;
      return `Ваш Возраст: ${Math.floor(age)};`;
    },

    getPassword() {
      let firstLetterOfName = firstName.slice(0, 1);
      return `Ваш пароль: ${firstLetterOfName.toUpperCase()}${lastName.toLowerCase()}${birthdayDate.getFullYear()}`;
    },
  };
}

const user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
