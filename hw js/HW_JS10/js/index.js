"use strict";

// const tabs = document.querySelectorAll(".tabs-title");
// const tabsTextColection = document.querySelectorAll(".tab");
// const currentTab = document.querySelector(".active");

// tabs.forEach((elem) => {
//   elem.addEventListener("click", () => {
//     tabsTextColection.forEach((elem) => {
//       elem.style.display = "none";
//     });

//     tabs.forEach((elem) => {
//       elem.classList.remove("active");
//     });

//     elem.classList.add("active");

//     tabs.forEach((elem, index) => {
//       if (elem.classList.contains("active")) {
//         document.querySelector(`.tab-${index}`).style.display = "block";
//       }
//     });
//   });
// });

const tabs = document.querySelector(".tabs");
const tabsColection = document.querySelectorAll(".tabs-title");
const tabsTextColection = document.querySelectorAll(".tab");
const currentTab = document.querySelector(".active");

tabs.addEventListener("click", (evt) => {
  tabsColection.forEach((elem) => {
    elem.classList.remove("active");
  });

  evt.target.classList.add("active");

  tabsTextColection.forEach((elem) => {
    elem.style.display = "none";
    if (elem.dataset.addTab === evt.target.dataset.addTab) {
      elem.style.display = "block";
    }
  });
});
