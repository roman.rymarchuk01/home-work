"use strict";

//1.setTimeout срабатывает один раз а setInterval работает пока его не очистят
//2. в таком случаи сначала выполниться весь код и только после выполниться функция setTimeout с задержкой в 0 поскольк
// при вызове этой функии ее колбек отходит в специальную облать браузера где ожидает своей очереди на выполнение
//3. Потому что setInterval несет в себе большую нагрузку на браузер

const imgWrapper = document.querySelector(".images-wrapper");
const stopBtn = document.querySelector(".stop");
const continueBtn = document.querySelector(".continue");

const swapImg = () => {
  const activImg = document.querySelector(".active");

  switch (activImg.dataset.index) {
    case "1":
      activImg.classList.remove("active");
      activImg.nextElementSibling.classList.add("active");
      break;
    case "2":
      activImg.classList.remove("active");
      activImg.nextElementSibling.classList.add("active");
      break;

    case "3":
      activImg.classList.remove("active");
      activImg.nextElementSibling.classList.add("active");
      break;

    case "4":
      activImg.classList.remove("active");
      imgWrapper.firstElementChild.classList.add("active");

      break;
  }
};

let swapImgInerval = setInterval(swapImg, 3000);
let flag = false;

stopBtn.addEventListener("click", () => {
  clearInterval(swapImgInerval);
  flag = true;
});

continueBtn.addEventListener("click", () => {
  if (flag === true) {
    swapImgInerval = setInterval(swapImg, 3000);
  }
  flag = false;
});
