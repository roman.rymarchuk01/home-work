"use strict";

const eyeImg = document.querySelectorAll(".fa-eye");
const password = document.querySelector(".password");
const repeatPassword = document.querySelector(".password-repeat");

// eyeImg.forEach((elem) => {
//   elem.addEventListener("click", (e) => {
//     if (elem.classList.contains("fa-eye")) {
//       elem.classList.replace("fa-eye", "fa-eye-slash");
//       elem.previousElementSibling.type = "text";
//     } else {
//       elem.classList.replace("fa-eye-slash", "fa-eye");
//       elem.previousElementSibling.type = "password";
//     }
//   });
// });

//____________________________________________________________________________________

const showOrHiddenPassword = (input) => {
  const eye = input.nextElementSibling;
  console.log(eye);
  if (eye.classList.contains("fa-eye")) {
    eye.classList.replace("fa-eye", "fa-eye-slash");
    input.type = "text";
  } else {
    eye.classList.replace("fa-eye-slash", "fa-eye");
    input.type = "password";
  }
};

password.nextElementSibling.addEventListener("click", () => {
  showOrHiddenPassword(password);
});
repeatPassword.nextElementSibling.addEventListener("click", () => {
  showOrHiddenPassword(repeatPassword);
});

//____________________________________________________________________________________

const submitBtn = document.querySelector(".btn");

submitBtn.addEventListener("click", (e) => {
  e.preventDefault();
  if (repeatPassword.value !== password.value) {
    document.querySelector(".error").innerText =
      "Нужно ввести одинаковые значения";
  } else {
    document.querySelector(".error").innerText = "";
    alert("You are welcome");
  }
});
