"use strict";
//1.DOM это дерево наследования HTML документа представленое в виде глобального обьекта
//2.innerHTML выводит все что вложено в элемент innerText выводит только текст из этого элемента
//3.К елементу можно обратиться через атрибуты name, class, id, через сам тег элемента и последний и удобнейший вариант обратиться по ccs селектору

const allP = document.querySelectorAll("p");
allP.forEach((p) => (p.style.backgroundColor = "#ff0000"));

const optionsListID = document.querySelectorAll("#optionsList");

optionsListID.forEach((elem) => {
  console.log(elem);
  console.log(elem.parentElement);
  if (elem.childNodes) {
    console.log(elem.childNodes);
    console.log(typeof elem.childNodes);
  }
});

document.querySelector("#testParagraph").innerHTML =
  "<p>This is a paragraph</p>";

const navLi = document.querySelectorAll(".main-header li");

navLi.forEach((li) => {
  li.classList.add("nav-item");
  console.log(li);
});

document
  .querySelectorAll(".section-title")
  ?.forEach((elem) => elem.classList.remove("section-title"));
