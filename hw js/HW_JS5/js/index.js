// 1. Методы обьектов это функции вложенные в область видемости определенного обьекта
// 2. Значения свойств в обьукте могут быть любового типа данных
// 3. Ссылочный тип данных означает что переменная обьекта не несет в себе полные значение этого обьекта, а несет в себе только ссылку на этот обьект

function createNewUser() {
  let firstName = prompt("Введите Имя");
  let lastName = prompt("Введите фамилию");
  const newUser = {
    firstName,
    lastName,
    getLogin() {
      let firstLetterOfName = firstName.slice(0, 1);
      let login = firstLetterOfName.concat(lastName);
      return login.toLowerCase();
    },
  };
  return newUser.getLogin();
}
console.log(createNewUser());
