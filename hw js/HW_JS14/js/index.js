const changeButton = document.querySelector(".documen-style");
const documentHTML = document.querySelector("html");

const setDarkStyle = () => {
  if (localStorage.getItem("style") === "dark") {
    documentHTML.classList = "dark";
  } else {
    documentHTML.classList = "";
  }
};

changeButton.addEventListener("click", () => {
  if (localStorage.getItem("style") === "dark") {
    localStorage.removeItem("style");
  } else {
    localStorage.setItem("style", "dark");
  }
  setDarkStyle();
});

setDarkStyle();
