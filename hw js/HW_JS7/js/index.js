"use strict";
//1. Переберает каждый елемент масива и выполняет переданую функцию к каждому из них
//2. Можно задать длену масива 0 и все элементы автоматом будут удалены так же как и их значения
//3. Определить переменная является масивом или нет можно с помощю метода isArray()

const array = [
  null,
  123,
  "Hello",
  true,
  {
    name: "Roman",
  },
];

const filterBy = (arr, type) => arr.filter((elem) => typeof elem != type);

console.log(filterBy(array, "string"));
