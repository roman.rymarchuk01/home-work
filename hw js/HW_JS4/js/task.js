//1.Функции позволяют нам сокращать и не дублировать код, в сучаи если в разных местах кода нам нужно выполнять одно и тоже действие
//2. Аргументы передют функии данные с которыми функция должна работать
//3. Оператор Return позволяет нам получить нужный нам конечный результат выполнения функции

let firstNum, secondNum, operation;

do {
  firstNum = prompt("ведите первое число", firstNum);
  secondNum = prompt("Введите второе число", secondNum);
} while (
  firstNum === "" ||
  firstNum === null ||
  isNaN(firstNum) ||
  secondNum === "" ||
  secondNum === null ||
  isNaN(secondNum)
);

function mathOperation(a, b, operation) {
  do {
    operation = prompt("Введите нужную операцию: +; -; *; /;");
  } while (
    operation !== "+" &&
    operation !== "*" &&
    operation !== "-" &&
    operation !== "/"
  );
  let res;
  switch (operation) {
    case "*":
      res = a * b;
      break;
    case "/":
      res = a / b;
      break;
    case "-":
      res = a - b;
      break;
    case "+":
      res = a + b;
      break;
  }
  return res;
}
console.log(mathOperation(+firstNum, +secondNum, operation));
