"use strict";

const getFibonachi = (f0, f1, n) => {
  if (n > 1) {
    return getFibonachi(f1, f0 + f1, n - 1);
  } else if (n < 0) {
    return getFibonachi(f1, f0 + f1, ++n) * -1;
  }
  return f0 + f1;
};
console.log(getFibonachi(0, 1, 6));
