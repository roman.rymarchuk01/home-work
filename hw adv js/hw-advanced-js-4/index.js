//AJAX очень важен для жизни сайта поскольку осуществляет обмен данными с удаленныйм сервером по запросу

"use strict";
const container = document.querySelector(".container");

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((r) => r.json())
  .then((re) => {
    re.forEach(({ name, episodeId, openingCrawl, characters }) => {
      container.insertAdjacentHTML(
        "beforeend",
        `<div class="card">
      <h2 class="title">Episode ${episodeId}: ${name}</h2>
      <p class="about">${openingCrawl}</p>
      <p class="characters-${episodeId}"><div id="preloader" class="visible"></div></p>
  </div>`
      );

      characters.forEach((el) =>
        fetch(el)
          .then((r) => r.json())
          .then((re) => re.name)
          .then(
            (res) =>
              (document.querySelector(`.characters-${episodeId}`).innerText +=
                res)
          )
          .catch((err) => console.warn(err))
      );
    });
  })
  .catch((err) => console.warn(err));
