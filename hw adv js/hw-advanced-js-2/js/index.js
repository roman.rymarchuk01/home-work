"use strict";

//например при получении данных с сервера, поскольку эти данные не всегда могут быть такими какими мы их ожидаем

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const list = document.createElement("ul");
list.classList.add("list");
document.querySelector("#root").append(list);

class Post {
  constructor(title, author, price) {
    this.listItem = document.createElement("li");
    this.titleH2 = document.createElement("h2");
    this.authorSpan = document.createElement("span");
    this.priceSpan = document.createElement("span");

    this.title = title;
    this.author = author;
    this.price = price;
  }
  render() {
    this.listItem.classList.add("post");
    this.authorSpan.classList.add("author");
    this.priceSpan.classList.add("price");
    this.titleH2.classList.add("title");

    this.titleH2.innerText = this.title;
    this.authorSpan.innerText = this.author;
    this.priceSpan.innerText = this.price;

    this.listItem.append(this.titleH2);
    this.listItem.append(this.authorSpan);
    this.listItem.append(this.priceSpan);
    list.append(this.listItem);
  }
}

class UndefinedValueError extends Error {
  constructor(value) {
    super();
    this.name = "UndefindeValueError";
    this.message = `${value} is undefined`;
  }
}

books.forEach((el) => {
  try {
    if (el.hasOwnProperty("name")) {
      if (el.hasOwnProperty("price")) {
        if (el.hasOwnProperty("author")) {
          new Post(el.name, el.author, el.price).render();
        } else {
          throw new UndefinedValueError("author");
        }
      } else {
        throw new UndefinedValueError("price");
      }
    } else {
      throw new UndefinedValueError("name");
    }
  } catch (err) {
    console.log(err);
  }
});
