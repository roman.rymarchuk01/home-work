"use strict";

const serchByIdBtn = document.querySelector(".serch-by-id");

serchByIdBtn.addEventListener("click", async () => {
  const { ip } = await fetch("https://api.ipify.org/?format=json")
    .then((r) => r.json())
    .catch((err) => console.warn(err));

  const { continent, city, country, regionName, district } = await fetch(
    `http://ip-api.com/json/${ip}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query`
  )
    .then((r) => r.json())
    .catch((err) => console.warn(err));

  document.querySelector(".user-info").innerHTML = `
    <p class="user-info__item">Континент: ${continent}</p>
    <p class="user-info__item">Країна: ${country}</p>
    <p class="user-info__item">Регіон: ${regionName}</p>
    <p class="user-info__item">Місто: ${city}</p>
    <p class="user-info__item">Район: ${district}</p>
`;
});
