"use strict";

//1.Прототипное наследование происходит от текущего обьекта по всем родителям,
//что в свою очередь означает что этому обьекту будут доступны все методы и элементы
//каждого из цепочки прототипного наследования

//2.Заимствует конструктор у класса родителя после чего можем его дополнить

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this._salary = salary;
  }

  get salary() {
    return this._salary;
  }

  set salary(newSalary) {
    return (this._salary = newSalary);
  }
}

const jhon = new Employee("jhon", 24, 1500);
console.log(jhon);
console.log(jhon.salary);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return super.salary * 3;
  }
}

const bob = new Programmer("bob", 20, 2000, "js");
const ron = new Programmer("ron", 22, 2500, "php");
const alla = new Programmer("alla", 25, 3000, "c++");

console.log(bob);
console.log(bob.salary);

console.log(ron);
console.log(ron.salary);

console.log(alla);
console.log(alla.salary);
