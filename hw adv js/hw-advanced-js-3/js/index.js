"use strict";

//деструктуризация это способ определить элементы обьектов и масивов в переменные более удобным и компактным способом

const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const arr = [...clients1, ...clients2];

const makeUniq = (arr) => {
  const uniqSet = new Set(arr);
  return [...uniqSet];
};

console.log(makeUniq(arr));

//_______________________________________________________________________________________________________________________

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

const newCharacters = characters.map(({ name, lastName, age }) => ({
  name,
  lastName,
  age,
}));

//_______________________________________________________________________________________________________________________

const user1 = {
  name: "John",
  years: 30,
};

const { name, years, isAdmin } = user1;

alert(`name: ${name}
age: ${years}
isAdmin: ${Boolean(isAdmin)}`);

//_______________________________________________________________________________________________________________________

const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const fullProfile = {
  ...satoshi2018,
  ...satoshi2019,
  ...satoshi2020,
};

console.log(fullProfile);

//_______________________________________________________________________________________________________________________

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

const newBooks = [...books, bookToAdd];

console.log(newBooks);

//_______________________________________________________________________________________________________________________

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const newEmployee = {
  ...employee,
  age: 50,
  salary: 30000,
};

console.log(newEmployee);

//_______________________________________________________________________________________________________________________

const [value, showValue] = ["value", () => "showValue"];

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'
